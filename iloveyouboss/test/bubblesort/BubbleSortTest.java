package bubblesort;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*; 
import org.junit.*;
import org.junit.Test;

public class BubbleSortTest {

	@Test
	public void BubbleSortWithFaultsSucess1() {
		int arr[] = {1, 2, 3, 4, 7, 7};
		int actArr[] = {1, 2, 3, 4, 7, 7};
		int n = arr.length;
		BubbleSort sort = new BubbleSort();
		sort.bubbleSort(arr, n);
		Assert.assertArrayEquals( actArr, arr );

	}
	@Test 
	public void BubbleSortWithFaultsSucess2() {
		
		int arr[] = {2, 4, 4, 6, 6, 9, 9, 10};
		int actArr[] = {2, 4, 4, 6, 6, 9, 9, 10};
		int n = arr.length;
		BubbleSort sort = new BubbleSort();
		sort.bubbleSort(arr, n);
		Assert.assertArrayEquals( actArr, arr );

	
	}
	@Test
	public void BubbleSortWithFaultsFailure1() {
		int arr[] = {27, 24, 25, 29, 30, 45};
		int actArr[] = {24, 25, 27, 29, 30, 45};
		int n = arr.length;
		BubbleSort sort = new BubbleSort();
		sort.bubbleSort(arr, n);
		Assert.assertArrayEquals( actArr, arr );

	}
	@Test
	public void BubbleSortWithFaultsFailure2() {
		int arr[] = {6, 5, 4, 3, 2, 1};
		int actArr[] = {1, 2, 3, 4, 5, 6};
		int n = arr.length;
		BubbleSort sort = new BubbleSort();
		sort.bubbleSort(arr, n);
		Assert.assertArrayEquals( actArr, arr );

	}

}
