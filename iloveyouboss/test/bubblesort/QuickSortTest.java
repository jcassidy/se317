package bubblesort;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class QuickSortTest {

	@Test
	public void QuickSortWithFaultsSucess1() {
		int arr[] = {64, 34, 25, 12, 22, 89, 90};
		int actArr[] = {12, 22, 25, 34, 64, 89, 90};
		int n = arr.length;
		QuickSort sort = new QuickSort();
		sort.quickSort(arr, 0, n-1);
		Assert.assertArrayEquals( actArr, arr );

	}
	@Test 
	public void QuickSortWithFaultsSucess2() {
		
		int arr[] = {6, 5, 3, 3, 5, 1, 7, 8};
		int actArr[] = {1, 3, 3, 5, 5, 6, 7, 8};
		int n = arr.length;
		QuickSort sort = new QuickSort();
		sort.quickSort(arr, 0, n-1);
		Assert.assertArrayEquals( actArr, arr );

	
	}
	@Test
	public void QuickSortWithFaultsFailure1() {
		int arr[] = {27, 24, 25, 30, 29, 45};
		int actArr[] = {24, 25, 27, 29, 30, 45};
		int n = arr.length;
		QuickSort sort = new QuickSort();
		sort.quickSort(arr, 0, n-1);
		Assert.assertArrayEquals( actArr, arr );

	}
	@Test
	public void QuickSortWithFaultsFailure2() {
		int arr[] = {6, 5, 4, 3, 2, 1};
		int actArr[] = {1, 2, 3, 4, 5, 6};
		int n = arr.length;
		QuickSort sort = new QuickSort();
		sort.quickSort(arr, 0, n-1);
		Assert.assertArrayEquals( actArr, arr );
	}

}
