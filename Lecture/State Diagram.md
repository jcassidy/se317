

```mermaid
stateDiagram-v2
[*] --> ItemsForSale
ItemsForSale --> AddToCart
AddToCart --> Checkout
Checkout --> Payment
Payment --> Authentication
Authentication --> InfoForUser
Authentication --> Payment : Authentication failed
InfoForUser --> OrderPlaced
OrderPlaced --> [*]
```
