
#### Types of test activities 
* Testing can be broken up into four general types of activities
1. Test Design
2. Test Automation
3. Test Execution
4. Test Evaluation 
* Each type of activity requires different skills, backgrounds knowledge, education, and training
* No reasonable software development organization uses the same people for requirements, design, implementation, integration and configuration control 



#### Test Design - Criteria-Based
* This is the most technical job in software testing
* Requires knowledge of:
	* Discrete math
	* Programming
	* Testing
* Requires much of a traditional CS degree
* This is intellectually stimulating, rewarding, and challenging
* Test Design is analogous to software architecture on the development side
* Using people who are not qualified to design tests is a sure way to get ineffective tests

#### Test Design - Human Based 
###### Design test values based on domain knowledge of the program and human knowledge of testing

* This is much harder than it may seem to developers
* Criteria-based approaches can be blind to special situations
* Requires knowledge of :
	* Domain, BP, testing, and user interfaces/interactions
* Requires almost no traditional CS
	* A background in the domain of the software is essential
	* An empirical background is very helpful
	* A logic background is very helpful

#### Test Automation
###### Embed test values into executable scripts

* This is slightly less technical
* Requires knowledge of programming
* Requires very little theory


#### Test Execution

###### Run tests on the software and records the results

* This is easy
* Requires basic computer skills
* Asking qualified test designers to execute tests is a sure way to convince them to look for a development job
* If GUI test are not well automated, this requires more manual labor

#### Test Evaluation


Test management: sets policy, organizes teams, interfaces with development, chooses criteria, decides how much automation is needed

Test maintenance : Save tests for reuse as software evolves 

Test Documentation : All parties participate
- Each test must document "why" - criterion and test requirement
- Ensure traceability throughout the process
- Keep documentation in the automated process

