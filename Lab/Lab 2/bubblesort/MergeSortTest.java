package bubblesort;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class MergeSortTest {

	@Test
	public void MergeSortWithFaultsSucess1() {
		int arr[] = {3, 4, 5, 6, 8, 7, 10};
		int actArr[] = {3, 4, 5, 6, 7, 8, 10};
		MergeSort sort = new MergeSort();
		sort.mergeSort(arr);
		Assert.assertArrayEquals( actArr, arr );

	}
	@Test 
	public void MergeSortWithFaultsSucess2() {
		
		int arr[] = {11,12,13,15,17};
		int actArr[] = {11,12,13,15,17};
		MergeSort sort = new MergeSort();
		sort.mergeSort(arr);
		Assert.assertArrayEquals( actArr, arr );

	
	}
	@Test
	public void MergeSortWithFaultsFailure1() {
		int arr[] = {27, 24, 25, 29, 30, 45};
		int actArr[] = {24, 25, 27, 29, 30, 45};
		int n = arr.length;
		MergeSort sort = new MergeSort();
		sort.mergeSort(arr);
		Assert.assertArrayEquals( actArr, arr );

	}
	@Test
	public void MergeSortWithFaultsFailure2() {
		int arr[] = {6, 5, 4, 3, 2, 1};
		int actArr[] = {1, 2, 3, 4, 5, 6};
		int n = arr.length;
		MergeSort sort = new MergeSort();
		sort.mergeSort(arr);
		Assert.assertArrayEquals( actArr, arr );

	}

}
