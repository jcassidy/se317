package bubblesort;

public class MergeSort {
    public static void main(String[] args) {
        int[] arr = {5, 8, 12, 18, 24, 7, 3};


        System.out.println("Original Array:");
        printArray(arr);
        MergeSort sort = new MergeSort();
        sort.mergeSort(arr);

        System.out.println("\nSorted Array (Merge Sort):");
        printArray(arr);
    }

    public void mergeSort(int[] arr) {
        int n = arr.length;

        if (n <= 1) { //fault added here, array thinks its sorted before it actually is 
            return; // The array is already sorted
        }

        // Find the middle of the array
        int mid = n / 2 ;  

        // Create left and right sub-arrays
        int[] left = new int[mid]; 
        int[] right = new int[n - mid ]; 

        // Copy data to left and right sub-arrays
        System.arraycopy(arr, 0, left, 0, mid);
        System.arraycopy(arr, mid, right, 0, n - mid);

        // Recursively sort both sub-arrays
        mergeSort(left);
        mergeSort(right);

        // Merge the sorted sub-arrays
        merge(arr, left, right);
    }

    public static void merge(int[] arr, int[] left, int[] right) {
        int n1 = left.length;
        int n2 = right.length;
        int i = 0, j = 0, k = 0;

        // Merge elements of left and right sub-arrays into the main array
        while (i < n1 && j < n2) {
            if (left[i] <= right[j]) {
                arr[k++] = left[i++];
            } else {
                arr[k++] = right[j++];
            }
        }

        // Copy remaining elements of left and right sub-arrays (if any)
        while (i < n1) {
            arr[k++] = left[i++];
        }

        while (j < n2) {
            arr[k++] = right[j++];
        }
    }

    public static void printArray(int[] arr) {
        for (int value : arr) {
            System.out.print(value + " ");
        }
        System.out.println();
    }
}

